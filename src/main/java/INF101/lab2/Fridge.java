package INF101.lab2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    List<FridgeItem> fridgeContent = new ArrayList<>();
    final int maxSize = 20;

    @Override
    public int nItemsInFridge() { return fridgeContent.size(); }

    @Override
    public int totalSize() { return maxSize; }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            fridgeContent.add(item);
        }
        return nItemsInFridge() < totalSize();
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeContent.contains(item)) {
            fridgeContent.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgeContent.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>(fridgeContent);
        expiredFood.removeIf(item -> item.hasExpired() != true);
        fridgeContent.removeIf(item -> item.hasExpired());
        return expiredFood;
    }
}